package com.ims.edm.dao;

import java.util.ArrayList;
import java.util.List;

import com.ims.edm.models.Employee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "employeedb";

	// employees table name
	private static final String TABLE_DIRECTORY = "directory";

	// DIRECTORY Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_FNAME = "fname";
	private static final String KEY_LNAME = "lname";
	private static final String KEY_PHONE = "phone";
	private static final String KEY_IMAGE = "image";

	public DataBaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_DIRECTORY_TABLE = "CREATE TABLE " + TABLE_DIRECTORY + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_FNAME + " TEXT,"
				+ KEY_LNAME + " TEXT," + KEY_PHONE + " TEXT," + KEY_IMAGE
				+ " BLOB" + ")";
		db.execSQL(CREATE_DIRECTORY_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIRECTORY);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	public boolean addEmployee(Employee employee) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FNAME, employee.getFname()); // employee first Name
		values.put(KEY_LNAME, employee.getLname()); // employee second name
		values.put(KEY_PHONE, employee.getContact()); // employee phone number
		values.put(KEY_IMAGE, employee.get_image()); // employee Photo

		// Inserting Row
		long l = db.insert(TABLE_DIRECTORY, null, values);
		db.close(); // Closing database connection
		if (l == -1) {
			return false;
		} else {
			return true;
		}
	}

	// Getting single employee
	public Employee getEmployee(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_DIRECTORY, new String[] { KEY_ID,
				KEY_FNAME, KEY_LNAME, KEY_PHONE, KEY_IMAGE }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Employee employee = new Employee(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getBlob(4));

		// return Employee
		return employee;

	}

	// Getting All Employees
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = new ArrayList<Employee>();
		// Select All Query
		String selectQuery = "SELECT * FROM directory ORDER BY fname";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Employee employee = new Employee();
				employee.set_id(Integer.parseInt(cursor.getString(0)));
				employee.setFname(cursor.getString(1));
				employee.setLname(cursor.getString(2));
				employee.set_image(cursor.getBlob(4));
				// Adding employee to list
				employeeList.add(employee);
			} while (cursor.moveToNext());
		}
		// close inserting data from database
		db.close();
		// return employee list
		return employeeList;

	}

	// Updating single employee
	public int updateEmployee(Employee employee) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FNAME, employee.getFname());
		values.put(KEY_LNAME, employee.getLname());
		values.put(KEY_PHONE, employee.getContact());
		values.put(KEY_IMAGE, employee.get_image());

		// updating row
		return db.update(TABLE_DIRECTORY, values, KEY_ID + " = ?",
				new String[] { String.valueOf(employee.get_id()) });

	}

	// Deleting single employee
	public void deleteEmployee(Employee employee) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_DIRECTORY, KEY_ID + " = ?",
				new String[] { String.valueOf(employee.get_id()) });
		db.close();
	}

	// Getting employees Count
	public int getEmployeesCount() {
		String countQuery = "SELECT  * FROM " + TABLE_DIRECTORY;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}
