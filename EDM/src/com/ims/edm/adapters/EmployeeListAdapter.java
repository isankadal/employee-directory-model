package com.ims.edm.adapters;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import com.ims.edm.R;
import com.ims.edm.dao.DataBaseHandler;
import com.ims.edm.models.Employee;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EmployeeListAdapter extends ArrayAdapter<Employee>{
	 Context context;
	 DataBaseHandler dbHandler;
	    int layoutResourceId;   
	   // BcardImage data[] = null;
	    ArrayList<Employee> data=new ArrayList<Employee>();
	    public EmployeeListAdapter(Context context, int layoutResourceId, ArrayList<Employee> data) {
	        super(context, layoutResourceId, data);
	        this.layoutResourceId = layoutResourceId;
	        dbHandler = new DataBaseHandler(context);
	        this.context = context;
	        this.data = data;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        ImageHolder holder = null;
	       
	        if(row == null)
	        {
	            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	            row = inflater.inflate(layoutResourceId, parent, false);
	           
	            holder = new ImageHolder();
	            holder.txtTitle = (TextView)row.findViewById(R.id.li_tv_employee_name);
	            holder.imgIcon = (ImageView)row.findViewById(R.id.li_iv_photo_preview);
	            holder.btnDelete = (ImageButton) row.findViewById(R.id.li_btn_delete);
	            //holder.btnDelete.setVisibility(View.GONE);
	            row.setTag(holder);
	        }
	        else
	        {
	            holder = (ImageHolder)row.getTag();
	        }
	       
	        Employee employee = data.get(position);
	        //holder.txtTitle.setText(employee.getFname() + " " + employee.getLname());
	        holder.txtTitle.setText(employee.getFname());
	        holder.btnDelete.setOnClickListener(new DeleteListener(employee));
	        
	        //convert byte to bitmap take from Employee class
	        byte[] outImage=employee.get_image();
	        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
	        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
	        holder.imgIcon.setImageBitmap(theImage);
	       return row;
	       
	    }
	    
	    private class DeleteListener implements OnClickListener{
	    	
	    	private Employee employee;
	    	
	    	public DeleteListener(Employee employee) {
				this.employee = employee;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(context, "Delete employee : " + employee.getFname(), Toast.LENGTH_SHORT).show();
				dbHandler.deleteEmployee(employee);
			}
	    	
	    }
	   
	    static class ImageHolder
	    {
	        ImageView imgIcon;
	        TextView txtTitle;
	        ImageButton btnDelete;
	    }
	}
