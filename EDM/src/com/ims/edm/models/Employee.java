package com.ims.edm.models;

public class Employee {

	// private variables
	private int _id;
	private String fname;
	private String lname;
	private String contact;
	private byte[] _image;

	// Empty constructor
	public Employee() {

	}

	// constructor
	public Employee(int keyId, String fname, String lname, String contact, byte[] image) {
		this._id = keyId;
		this.fname = fname;
		this.lname = lname;
		this.contact = contact;
		this._image = image;

	}
	public Employee(String fname, String lname, String contact, byte[] image) {
		this.fname = fname;
		this.lname = lname;
		this.contact = contact;
		this._image = image;

	}	
	public Employee(int keyId, String fname, String lname, byte[] image){
		this._id = keyId;
		this.fname = fname;
		this.lname = lname;
		this._image = image;
	}
	public Employee(int keyId) {
		this._id = keyId;

	}

	//get id
	public int get_id() {
		return _id;
	}

	//set id
	public void set_id(int _id) {
		this._id = _id;
	}

	//get first name
	public String getFname() {
		return fname;
	}

	//set first name
	public void setFname(String fname) {
		this.fname = fname;
	}

	//get last name
	public String getLname() {
		return lname;
	}

	//set last name
	public void setLname(String lname) {
		this.lname = lname;
	}

	//get contact
	public String getContact() {
		return contact;
	}

	//set contact
	public void setContact(String contact) {
		this.contact = contact;
	}

	//get image
	public byte[] get_image() {
		return _image;
	}

	//set image
	public void set_image(byte[] _image) {
		this._image = _image;
	}

}
