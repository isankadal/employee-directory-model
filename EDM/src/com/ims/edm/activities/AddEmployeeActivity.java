package com.ims.edm.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.ims.edm.R;
import com.ims.edm.dao.DataBaseHandler;
import com.ims.edm.models.Employee;
import com.ims.edm.utility.Helper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class AddEmployeeActivity extends Activity implements ValidationListener {

	@Required(order = 1)
	private EditText et_employee_fname;

	@Required(order = 2)
	private EditText et_employee_lname;

	@Required(order = 3)
	@TextRule(order = 4, minLength = 10, maxLength = 10, message = "require 10 digit number")
	private EditText et_employee_number;

	private String _imageCapturedName = null;
	private Bitmap bitmap = null;
	private final int TAKE_FRONT_PHOTO = 1;
	private ImageView iv_employee_image;
	private ByteArrayOutputStream bytes;
	private DataBaseHandler dbHandler;
	private Employee employee;
	private Validator validator;
	private String mCurrentPhotoPath; // File path to the last image captured

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_employee);

		init(savedInstanceState);

	}

	public void init(Bundle savedInstanceState) {
		validator = new Validator(this);
		
		//create DatabaseHandler object
        dbHandler = new DataBaseHandler(getApplicationContext());

		et_employee_fname = (EditText) findViewById(R.id.et_employee_fname);
		et_employee_lname = (EditText) findViewById(R.id.et_employee_lname);
		et_employee_number = (EditText) findViewById(R.id.et_employee_number);
		iv_employee_image = (ImageView) findViewById(R.id.iv_employee_image);
		if (savedInstanceState != null) {
			bitmap = (Bitmap)savedInstanceState.getParcelable("image");
			if (bitmap != null) {
				iv_employee_image.setImageBitmap(bitmap);
				bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
				mCurrentPhotoPath = savedInstanceState.getString("imagepath");
			}
			
		}
		employee = new Employee();

		validator.setValidationListener(this);
	}

	public void saveEmployee(View view) {
		validator.validate();
	}

	public void takePicture(View view) {
		// Code to get image from camera source
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		if (intent.resolveActivity(getPackageManager()) != null) {
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException e) {
				// Error occurred while creating the File
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				// ******** code for crop image
				intent.putExtra("crop", "true");
				intent.putExtra("aspectX", 0);
				intent.putExtra("aspectY", 0);
				intent.putExtra("outputX", 300);
				intent.putExtra("outputY", 300);
				startActivityForResult(intent, TAKE_FRONT_PHOTO);
			}

		}

	}

	private File createImageFile() throws IOException {
		// Create an image file name
		_imageCapturedName = "Image_"
				+ String.valueOf(System.currentTimeMillis());
		File image = Helper.createFileInSDCard(Helper.getTempFile()
				+ "TempFolder/", _imageCapturedName + ".jpg");

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		return image;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == TAKE_FRONT_PHOTO && resultCode == RESULT_OK) {
			try {
				File f = new File(mCurrentPhotoPath);

				bitmap = Helper.decodeFile(f);
				bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, true);
				bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);

				File file1 = new File(f.toString());
				FileOutputStream fo = new FileOutputStream(file1);
				fo.write(bytes.toByteArray());

				fo.flush();
				fo.close();

				iv_employee_image.setImageBitmap(bitmap);
				
			} catch (Exception e) {
				e.printStackTrace();
			} catch (OutOfMemoryError o) {
				o.printStackTrace();

			}

		}
	}

	public void makeToast(String charSequense) {
		Toast.makeText(this, charSequense, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onValidationSucceeded() {
		// Code to save data to sqlite database
		String first_name = et_employee_fname.getText().toString();
		String last_name = et_employee_lname.getText().toString();
		String phone_number = et_employee_number.getText().toString();
		byte[] employe_image_bytes = null;
		if (bitmap != null) {
			employe_image_bytes = bytes.toByteArray();
			File f = new File(mCurrentPhotoPath);
			f.delete();
		}else{
			//Convert default image to byte array
			Drawable d = getResources().getDrawable(R.drawable.default_image);
			Bitmap defaultImageBitmap = ((BitmapDrawable)d).getBitmap();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			defaultImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		    employe_image_bytes = stream.toByteArray();
		}
		
		employee = new Employee(first_name, last_name, phone_number, employe_image_bytes);
		
		boolean result = dbHandler.addEmployee(employee);
		if (result) {
			makeToast("Employee details added in db");
			finish();
		} else {
			makeToast("Error in adding details");
		}

	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (bitmap != null) {
			outState.putParcelable("image", bitmap);
			outState.putString("imagepath", mCurrentPhotoPath);
		}
		
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		bitmap = savedInstanceState.getParcelable("image");
		mCurrentPhotoPath = savedInstanceState.getString("imagepath");		
	}

}
