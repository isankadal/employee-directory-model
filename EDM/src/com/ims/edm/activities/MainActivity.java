package com.ims.edm.activities;

import java.util.ArrayList;
import java.util.List;

import com.ims.edm.R;
import com.ims.edm.adapters.EmployeeListAdapter;
import com.ims.edm.dao.DataBaseHandler;
import com.ims.edm.models.Employee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class MainActivity extends Activity implements OnItemClickListener{
	
	private ArrayList<Employee> employees = new ArrayList<Employee>();
	private EmployeeListAdapter listAdapter;
	private ListView employeesListView;
	private DataBaseHandler dbHandler;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        employeesListView = (ListView) findViewById(R.id.lv_employee_list);
        
        //create DatabaseHandler object
        dbHandler = new DataBaseHandler(getApplicationContext());
        
        employeesListView.setOnItemClickListener(this);
        
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	
    	fetchData(); //get data from database
        
        //Set Data base Item into listview
        listAdapter = new EmployeeListAdapter(this, R.layout.employee_list_item, employees);
        
        employeesListView.setAdapter(listAdapter);
        
    }
    
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	listAdapter.clear();
    }
    
    public void fetchData(){
    	//reading and getting all regards from database
        List<Employee> employeesList = dbHandler.getAllEmployees();
        for (Employee employee : employeesList) {
        	String log = "ID:" + employee.get_id() + " Name: " + employee.getFname()
					+ " ,Image: " + employee.get_image();

			// Writing Employee to log
			Log.d("Result: ", log);
			
			// add Employee data in arrayList
			employees.add(employee);
		}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
        	//Invoke AddNewEmployee activity     	
        	Intent addEmployeeIntent = new Intent(getBaseContext(), AddEmployeeActivity.class);
        	startActivity(addEmployeeIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent detailsIntent = new Intent(getBaseContext(), Details.class);
		detailsIntent.putExtra("empid", employees.get(position).get_id());
		startActivity(detailsIntent);
	}
}
