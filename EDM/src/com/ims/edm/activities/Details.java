package com.ims.edm.activities;

import java.io.ByteArrayInputStream;

import com.ims.edm.R;
import com.ims.edm.dao.DataBaseHandler;
import com.ims.edm.models.Employee;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Details extends Activity {
	
	private ImageView employeeImage;
	private TextView employeeName;
	private TextView employeePhoneNumber;
	
	private DataBaseHandler dbHandler;
	private Employee employee;
	private int id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		
		employeeImage = (ImageView) findViewById(R.id.iv_detail_image);
		employeeName = (TextView) findViewById(R.id.tv_detail_name);
		employeePhoneNumber = (TextView) findViewById(R.id.tv_detail_phone);
		
		dbHandler = new DataBaseHandler(getApplicationContext());
		id = getIntent().getIntExtra("empid", 0);
		
		//Get employee detatils
		employee = dbHandler.getEmployee(id);
		
		employeeImage.setImageBitmap(convertByteArry2Bitmap(employee.get_image()));
		employeeName.setText(employee.getFname() + " " + employee.getLname());
		employeePhoneNumber.setText(employee.getContact());
		
	}
	
	//convert byte to bitmap take from Employee class
	public Bitmap convertByteArry2Bitmap(byte[] array){
		
		ByteArrayInputStream imageStream = new ByteArrayInputStream(array);
        Bitmap image = BitmapFactory.decodeStream(imageStream);
        
		return image;
	}
	
	public void deleteEmployee(View view){
		dbHandler.deleteEmployee(employee);
		finish();
	}
}
